(function ($) {
    $.fn.select3 = function (options) {
        let settings = $.extend({
            'background-color': '#1E90FF',
            'color': 'white',
            'size': 7
        }, options);
        return this.each(()=>{
            this.css({width:'100%'});
            this.addClass('form-control');
            this.wrap('<div class="wrapper_select_filter" style="height: 37px;">');
            let parent = this.parent();
            parent.css({position: 'relative'}).append('<input type="text" class="input_select_filter form-control">');
            let input = parent.find('.input_select_filter');
            input.css({position: 'absolute', top: 0, left:0, width: 'calc(100% - 24px)'});
            input.val($(this).find('option:selected').text());

            let findMatches = ()=> {
                let filtred_select = $('.filtred_select');
                filtred_select.find('option').remove();
                $(this).find('option').each((i, el)=>{
                    if(($(el).text()).toLowerCase().indexOf(input.val().toLowerCase()) >= 0){
                        filtred_select.append(`<option value="${$(el).attr('value')}">${$(el).text()}</option>`);
                    }
                });
                filtred_select.find('option').first().addClass('active_option').css({'background-color': settings['background-color'], 'color': settings['color']});

            };

            let setChoosedOption = ()=> {
                input.val($('.filtred_select').find('option:selected').text());
                $(this).val($('.filtred_select').val()).trigger('change');
                try{
                    $('.filtred_select').remove();
                } catch (e) {
                    console.log(e);
                }
            };

            $(this).on('change', ()=>{
                input.val($(this).find('option:selected').text());
            });

            input.on('focus', ()=>{
                input.select();
                parent.append('<select class="filtred_select">');
                $('.filtred_select').css({position:'absolute', top:'100%', left:'0', zIndex:'999', width:'100%'}).attr('size', settings['size']);
                findMatches();
            });

            input.on('blur', (e)=>{
                if(!$(e.relatedTarget).hasClass('filtred_select')){
                    $('.filtred_select').remove();
                }
            });

            $(document).on('blur','.filtred_select', (e)=>{
                if(!$(e.relatedTarget).hasClass('input_select_filter')){
                    $('.filtred_select').remove();
                }
            });

            input.on('keyup', (e)=>{
                switch (e.keyCode){
                    case 13:
                        //Enter
                        $('.filtred_select .active_option').prop('selected', true);
                        setChoosedOption();
                        break;
                    case 27:
                        //Esc
                        input.val('');
                        break;
                    case 38:
                        //KeyUp
                        e.stopPropagation();
                        break;
                    case 40:
                        //KeyDown
                        e.stopPropagation();
                        break;
                    default:
                        findMatches();
                        break;
                }

            });
            input.on('keydown', (e)=>{
                let filtred_select = $('.filtred_select');
                let height_option = 0;
                switch (e.keyCode){
                    case 13:
                        //Enter
                        break;
                    case 27:
                        //Esc
                        break;
                    case 38:
                        //KeyUp
                        height_option = $(filtred_select.find('option').get(0)).outerHeight();
                        filtred_select.scrollTop(filtred_select.scrollTop() - height_option);
                        if(!filtred_select.find('option').first().hasClass('active_option')) {
                            filtred_select.find('option').each((i, el) => {
                                let $el = $(el);
                                if ($el.hasClass('active_option')) {
                                    $el.removeClass('active_option').css({
                                        'background-color': 'inherit',
                                        'color': 'inherit'
                                    });
                                    filtred_select.find('option').eq(i - 1).addClass('active_option').css({
                                        'background-color': settings['background-color'],
                                        'color': settings['color']
                                    });

                                    return false;
                                }
                            });
                        }
                        break;
                    case 40:
                        //KeyDown
                        height_option = $(filtred_select.find('option').get(0)).outerHeight();
                        filtred_select.scrollTop(filtred_select.scrollTop() + height_option);
                        if(!filtred_select.find('option').last().hasClass('active_option')){
                            filtred_select.find('option').each((i, el)=>{
                                let $el = $(el);
                                if($el.hasClass('active_option')) {
                                    $el.removeClass('active_option').css({
                                        'background-color': 'inherit',
                                        'color': 'inherit'
                                    });
                                    filtred_select.find('option').eq(i + 1).addClass('active_option').css({
                                        'background-color': settings['background-color'],
                                        'color': settings['color']
                                    });

                                    return false;
                                }
                            });
                        }

                        break;
                    default:
                        e.stopPropagation();
                        break;
                }

            });

            $(document).on('click','.filtred_select option', (e)=>{
                $('.filtred_select .active_option').removeClass('active_option').css({'background-color': 'inherit', 'color': 'inherit'});
                setChoosedOption();
            });
        });
    };
})(jQuery);
